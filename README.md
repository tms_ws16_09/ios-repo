# christmas.io

Weihnachtsmärkte in Berlin/Brandenburg finden leicht gemacht!

## Getting Started

Im Folgenden wird erklärt wie man die benutzten Entwicklungsumgebungen für die Entwicklung von christmas.io konfiguriert.
Außerdem ist beschrieben, wie das Ausführen der entwickelten Tests für das Produkt abläuft.

### Vorbereitungen

Es muss folgende Software in der jeweils aktuellen Version (oder der unten angegebenen) installiert sein

```
XCode 8
```

Außerdem muss zum erfolgreichen Ausführen der App eine Internetverbindung verfügbar sein, da die verwendeten Daten aus dem Internet bezogen werden.

### Installation

Eine Schritt-Für-Schritt Anleitung welche Dir hilft die Entwicklungsumgebung aufzusetzen

Folgende Schritte müssen ausgeführt werden

```
XCode 8 installieren
```

```
Projekt klonen
```

```
Abhängigkeiten installieren: pod install
```

## Ausführen der Tests

Um Tests an dem System durchzuführen werden im Emulator vom Benutzer Eingaben getätigt.

## Deployment

Die App läuft auf allen emulierten iOS Geräten mit iOS7 oder höher.

## Erstellt mit

* [XCode](https://developer.apple.com/xcode/) - Verwendete IDE

## Versionierung

Wir benutzen [Bitbucket](http://bitbucket.org/) zur Versionierung. Für eine Übersicht aller verfügbaren Versionen, klicke [hier](https://bitbucket.org/tms_ws16_09/ios-repo).

## Autoren

* **Adam Sasin** - *Entwickler* - [AdamSasin](https://bitbucket.org/brudi95/)

## Lizenz

Das Produkt unterliegt derzeit keiner Lizenz.

## Erwähnungen

* Danke an alle Autoren von genutzten Fremd-Libraries
