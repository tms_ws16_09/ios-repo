//
//  DetailViewController.swift
//  christmas.io
//
//  Created by Adam Sasin on 11/01/17.
//  Copyright © 2017 Adam Sasin. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    var stationName: String?
    var station: Station?

    @IBOutlet var name: UILabel!
    @IBOutlet var strasse: UILabel!
    @IBOutlet var platz: UILabel!
    @IBOutlet var vonbis: UILabel!
    @IBOutlet var oeffnungszeiten: UILabel!
    @IBOutlet var email: UILabel!
    @IBOutlet var w3: UILabel!
    @IBOutlet var bemerkungen: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setStation()
        
        //print(anno?.bezirk)
        
        if let val = station?.title {
            name.text = val
        }
        
        if let val = station?.strasse {
            strasse.text = val
        }
        
        if let val = station?.plz_ort {
            platz.text = val
        }
        
        if let val = station?.von, let val1 = station?.bis {
            vonbis.text = val + val1
        }
        
        if let val = station?.oeffnungszeiten {
            oeffnungszeiten.text = val
        }
        
        if let val = station?.email {
            email.text = val
        }
        
        if let val = station?.w3 {
            w3.text = val
        }
        
        if let val = station?.bemerkungen {
            bemerkungen.text = val
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setStation() {
        var done: Bool = false
        
        for annotation in Content.annotations {
            if annotation.title == stationName {
                station = annotation
                done = true
            }
            
            if (done) {
                break
            }
        }
    }
    
    @IBAction func openNavi(_ sender: Any) {
        // Open and show coordinate
        //let url = "http://maps.apple.com/maps?saddr=\(station?.lat),\(station?.long)"
        //UIApplication.shared.openURL(URL(string:url)!)
        
        if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
            UIApplication.shared.openURL(NSURL(string:
                "comgooglemaps://?center=\(station?.lat),\(station?.long)&zoom=14&views=traffic")! as URL)
        } else {
            print("Can't use comgooglemaps://");
        }

    }
        // Navigate from one coordinate to another
        //let url = "http://maps.apple.com/maps?saddr=\(from.latitude),\(from.longitude)&daddr=\(to.latitude),\(to.longitude)"
        //UIApplication.shared.openURL(URL(string:url)!)
}

