//
//  TabBarController.swift
//  christmas.io
//
//  Created by Adam Sasin on 11/01/17.
//  Copyright © 2017 Adam Sasin. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let items = self.tabBar.items
        {
            var count: Int = 1
            
            for item in items
            {
                if let image = item.image
                {
                    if count % 2 == 0 {
                        item.image = UIImage(named:"list-unselected")?.withRenderingMode(.alwaysOriginal)
                        item.selectedImage = UIImage(named:"list")?.withRenderingMode(.alwaysOriginal)
                    } else {
                        item.image = UIImage(named:"card-unselected")?.withRenderingMode(.alwaysOriginal)
                        item.selectedImage = UIImage(named:"card")?.withRenderingMode(.alwaysOriginal)
                    }
                }
                
                count += 1
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
}

extension UIColor {
    
    /**
     Creates a UIColor object for the given rgb value which can be specified
     as HTML hex color value. For example:
     
     let color = UIColor(rgb: 0x8046A2)
     let colorWithAlpha = UIColor(rgb: 0x8046A2, alpha: 0.5)
     
     - parameter rgb: color value as Int. To be specified as hex literal like 0xff00ff
     - parameter alpha: alpha optional alpha value (default 1.0)
     */
    convenience init(rgb: Int, alpha: CGFloat = 1.0) {
        let r = CGFloat((rgb & 0xff0000) >> 16) / 255
        let g = CGFloat((rgb & 0x00ff00) >>  8) / 255
        let b = CGFloat((rgb & 0x0000ff)      ) / 255
        
        self.init(red: r, green: g, blue: b, alpha: alpha)
    }
    
}
