//
//  MainViewController.swift
//  christmas.io
//
//  Created by Adam Sasin on 11/01/17.
//  Copyright © 2017 Adam Sasin. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet var scrollView: UIScrollView!
    
    var currentViewIsMap: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.delegate = self
        
        var V1: FilterController = FilterController(nibName: "FilterController", bundle: nil)
        var V2: MapViewController = MapViewController(nibName: "MapViewController", bundle: nil)
        
        self.addChildViewController(V1)
        self.scrollView.addSubview(V1.view)
        V1.didMove(toParentViewController: self)
        
        self.addChildViewController(V2)
        self.scrollView.addSubview(V2.view)
        V2.didMove(toParentViewController: self)
        
        var V2Frame: CGRect = V2.view.frame
        V2Frame.origin.x = self.view.frame.width
        V2.view.frame = V2Frame
        
        self.scrollView.contentSize = CGSize(width: self.view.frame.width * 2, height: self.view.frame.size.height)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func swipeToFilter(_ sender: Any) {
        if self.scrollView.contentOffset.x == 0.0 {
            self.scrollView.scrollRectToVisible(CGRect(x: self.view.frame.width, y: 0, width: self.view.frame.width, height: self.view.frame.size.height), animated: true)
        } else {
            self.scrollView.scrollRectToVisible(CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.size.height), animated: true)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        //print("Scroll finished")
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        /*print("something finished")
        print(self.scrollView.contentOffset.x)
        print(self.scrollView.contentOffset.y)
        
        if self.scrollView.contentOffset.x == 0.0 {
            currentViewIsMap = true
            configureMenu()
        } else {
            currentViewIsMap = false
            configureMenu()
        }*/
    }

}
