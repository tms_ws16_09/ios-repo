//
//  FirstViewController.swift
//  christmas.io
//
//  Created by Adam Sasin on 07/01/17.
//  Copyright © 2017 Adam Sasin. All rights reserved.
//

import UIKit
import MapKit
import Alamofire
import SwiftyJSON

class FirstViewController: UIViewController, MKMapViewDelegate  {
        
    @IBOutlet var mapView: MKMapView!
    
    var overlay : UIView?
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    
    let startLocation = CLLocation(latitude: 52.517138, longitude: 13.401489)
    let regionRadius: CLLocationDistance = 20000
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
        mapView.showsUserLocation = true
        
        centerMapOnLocation(location: startLocation)
        
        getData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func getData() {
        showOverlay()
        showIndicator()
        
        NetworkManager.shared().getData(place: Place.Berlin)
        NetworkManager.shared().getData(place: Place.Brandenburg)
        
        NetworkManager.shared().dispatchGroup.notify(queue: DispatchQueue.main) {
            self.hideIndicator()
            self.hideOverlay()
            
            self.mapView.removeAnnotations(Content.annotations)
            self.mapView.addAnnotations(Content.annotations)
        }
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let region = MKCoordinateRegionMakeWithDistance(startLocation.coordinate, regionRadius, regionRadius)
        mapView.setRegion(region, animated: true)
    }
    
    @IBAction func zoomToUser(sender: AnyObject) {
        let userLocation = mapView.userLocation.coordinate
        
        let region = MKCoordinateRegionMakeWithDistance(userLocation, 2000, 2000)
        //self.mapView.removeAnnotations(Content.annotations)
        //self.mapView.addAnnotations(Content.annotations)
        mapView.setRegion(region, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        //print("did change")
    }
    
    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        //print("will change")
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if let annotation = view.annotation {
            stationName = annotation.title!
        }
    }
    
    var stationName: String?

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {

        // Don't want to show a custom image if the annotation is the user's location.
        guard !(annotation is MKUserLocation) else {
            return nil
        }
    
        // Better to make this class property
        let annotationIdentifier = "identifier"
        
        var annotationView: MKAnnotationView?
        if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
            annotationView = dequeuedAnnotationView
            annotationView?.annotation = annotation
        }
        else {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        
        if let annotationView = annotationView {
            // Configure your annotation view here
            annotationView.canShowCallout = true
                        
            for anno in Content.annotations {
                guard let a1 = anno.title, !a1.isEmpty  else {
                    print("String is nil or empty.")
                    break // or break, continue, throw
                }
                
                guard let a2 = annotation.title, !(a2?.isEmpty)!  else {
                    print("String is nil or empty.")
                    break // or break, continue, throw
                }

                if a1 == a2 {
                    annotationView.image = UIImage(named: anno.image!)
                }
            }
        }
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        /*if control == view.rightCalloutAccessoryView {
            print("1")
            performSegue(withIdentifier: "toTheMoon", sender: view)
        }*/
        /*let capital = view.annotation as! Station
        let placeName = capital.title
        let placeInfo = "some"
        
        let ac = UIAlertController(title: placeName, message: placeInfo, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)*/
        
        if control == view.rightCalloutAccessoryView {
            performSegue(withIdentifier: "toDetail", sender: self)
        }
    }
    
    /*@IBAction func didReturnToMapViewController(_ segue: UIStoryboardSegue) {
        print(#function)
    }*/
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "toDetail" )
        {
            var dvc = segue.destination as! DetailViewController
            
            dvc.stationName = stationName
        }
    }
    
    func showOverlay() {
        overlay = UIView(frame: view.frame)
        overlay!.backgroundColor = UIColor.black
        overlay!.alpha = 0.8
        
        //self.tabBarController?.tabBar.layer.zPosition = -1
        view.addSubview(overlay!)
    }
    
    func hideOverlay() {
        overlay?.removeFromSuperview()
        //self.tabBarController?.tabBar.layer.zPosition = 1
    }
    
    func showIndicator() {
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0);
        actInd.center = view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        view.addSubview(actInd)
        actInd.startAnimating()
    }
    
    func hideIndicator() {
        actInd.stopAnimating()
        actInd.removeFromSuperview()
    }

}
