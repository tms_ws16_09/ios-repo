//
//  ContentManager.swift
//  christmas.io
//
//  Created by Adam Sasin on 14/01/17.
//  Copyright © 2017 Adam Sasin. All rights reserved.
//

import Foundation
import MapKit
import Alamofire
import SwiftyJSON

class NetworkManager {
    
    public private(set) var queue: DispatchQueue = DispatchQueue(label: "com.adamsasin.christmas.dispatchgroups", attributes: .concurrent, target: .main)
    public private(set) var dispatchGroup: DispatchGroup = DispatchGroup()
    
    private static var sharedNetworkManager: NetworkManager = {
        let networkManager = NetworkManager()
        
        // Configuration
        // ...
        
        return networkManager
    }()
    //private static var sharedNetworkManager = NetworkManager()
    
    private init() {}
    
    class func shared() -> NetworkManager {
        return sharedNetworkManager
    }
    
    public func getData(place: Place) {
        var url: String
        
        if place == Place.Brandenburg {
            url = Content.brandenburgUrl
        } else {
            url = Content.berlinUrl
        }
        
        dispatchGroup.enter()
        
        queue.async(group: dispatchGroup) {
            Alamofire.request(url).responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    if let markets = json["index"].array {
                        for market in markets {
                            /*guard !(market["name"].string?.isEmpty)!,
                            !(market["strasse"].string?.isEmpty)!,
                            !(market["plz_ort"].string?.isEmpty)!,
                            !(market["veranstalter"].string?.isEmpty)!,
                            !(market["von"].string?.isEmpty)!,
                            !(market["bis"].string?.isEmpty)!,
                            !(market["oeffnungszeiten"].string?.isEmpty)!,
                            !(market["email"].string?.isEmpty)!,
                            !(market["bemerkungen"].string?.isEmpty)!,
                            !(market["lat"].string?.isEmpty)!,
                            !(market["lng"].string?.isEmpty)!
                            else {
                                break
                            }*/
                            
                            let latD = Double(String(format:"%.10f", (market["lat"].string?.floatConverter)!))
                            let lngD = Double(String(format:"%.10f", (market["lng"].string?.floatConverter)!))
                            
                            var station: Station = Station(title: market["name"].string!, subtitle: "", lat: latD!, long: lngD!);
                            
                            if place == Place.Brandenburg {
                                station.image = "marker-blue"
                            } else {
                                station.image = "marker-red"
                            }
                            
                            guard let street = market["strasse"].string else {
                                break
                            }
                            station.strasse = street
                            
                            guard let plz_ort = market["plz_ort"].string else {
                                break
                            }
                            station.plz_ort = plz_ort
                            
                            guard let von = market["von"].string else {
                                break
                            }
                            station.von = von
                            
                            guard let bis = market["bis"].string else {
                                break
                            }
                            station.bis = bis
                            
                            guard let oeffnungszeiten = market["oeffnungszeiten"].string else {
                                break
                            }
                            station.oeffnungszeiten = oeffnungszeiten
                            
                            guard let email = market["email"].string else {
                                break
                            }
                            station.email = email
                            
                            guard let w3 = market["w3"].string else {
                                break
                            }
                            station.w3 = w3
                            
                            guard let bemerkungen = market["bemerkungen"].string else {
                                break
                            }
                            station.bemerkungen = bemerkungen
                            
                            Content.annotations += [station]
                        }
                    }
                    
                case .failure(let error):
                    print("error: ",error)
                }
                
                self.dispatchGroup.leave()
            }
        }
    }
    
}

struct Content {
    
    static var berlinUrl = "http://www.berlin.de/sen/wirtschaft/service/maerkte-feste/weihnachtsmaerkte/index.php/index.json?page=1"
    
    static var brandenburgUrl = "http://www.berlin.de/sen/wirtschaft/service/maerkte-feste/weihnachtsmaerkte/brandenburger-weihnachtsmaerkte/index.php/index.json?page=1"
    
    static var annotations = [Station]()
    
}

class Station: NSObject, MKAnnotation {
    
    var identifier = "identifier"
    
    var title: String?
    var subtitle: String?
    var lat: String?
    var long: String?
    var coordinate: CLLocationCoordinate2D
    
    private var _strasse: String? = nil
    var strasse: String? {
        set {
            self._strasse = newValue
        }
        get {
            return self._strasse
        }
    }
    
    private var _bezirk: String? = nil
    var bezirk: String? {
        set {
            self._bezirk = newValue
        }
        get {
            return self._bezirk
        }
    }
    
    private var _plz_ort: String? = nil
    var plz_ort: String? {
        set {
            self._plz_ort = newValue
        }
        get {
            return self._plz_ort
        }
    }
    
    private var _veranstalter: String? = nil
    var veranstalter: String? {
        set {
            self._veranstalter = newValue
        }
        get {
            return self._veranstalter
        }
    }
    
    private var _von: String? = nil
    var von: String? {
        set {
            self._von = newValue
        }
        get {
            return self._von
        }
    }
    
    private var _bis: String? = nil
    var bis: String? {
        set {
            self._bis = newValue
        }
        get {
            return self._bis
        }
    }
    
    private var _oeffnungszeiten: String? = nil
    var oeffnungszeiten: String? {
        set {
            self._oeffnungszeiten = newValue
        }
        get {
            return self._oeffnungszeiten
        }
    }
    
    private var _email: String? = nil
    var email: String? {
        set {
            self._email = newValue
        }
        get {
            return self._email
        }
    }
    
    private var _w3: String? = nil
    var w3: String? {
        set {
            self._w3 = newValue
        }
        get {
            return self._w3
        }
    }
    
    private var _bemerkungen: String? = nil
    var bemerkungen: String? {
        set {
            self._bemerkungen = newValue
        }
        get {
            return self._bemerkungen
        }
    }
    
    private var _image: String? = nil
    var image: String? {
        set {
            self._image = newValue
        }
        get {
            return self._image
        }
    }
    
    init (title: String, subtitle: String, lat: CLLocationDegrees, long: CLLocationDegrees) {
        self.title = title
        self.subtitle = subtitle
        self.coordinate = CLLocationCoordinate2DMake(lat, long)
    }
    
}

enum Place {
    case Berlin
    case Brandenburg
}

extension String {
    var floatConverter: Float {
        let converter = NumberFormatter()
        converter.decimalSeparator = "."
        if let result = converter.number(from: self) {
            return result.floatValue
        } else {
            converter.decimalSeparator = ","
            if let result = converter.number(from: self) {
                return result.floatValue
            }
        }
        return 0
    }
}
