//
//  SecondViewController.swift
//  christmas.io
//
//  Created by Adam Sasin on 07/01/17.
//  Copyright © 2017 Adam Sasin. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SecondViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var tableView: UITableView!
    
    var overlay : UIView?
    var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Content.annotations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "station", for: indexPath)
        
        //cell.textLabel?.text = "Section \(indexPath.section) Row \(indexPath.row)"
        cell.textLabel?.text = Content.annotations[indexPath.row].title
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You selected cell #\(indexPath.row)!")
        performSegue(withIdentifier: "detail", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var indexPath: NSIndexPath = self.tableView.indexPathForSelectedRow! as NSIndexPath
        
        var dest: DetailViewController = segue.destination as! DetailViewController
        
        dest.stationName = Content.annotations[indexPath.row].title
    }
    
    func showOverlay() {
        overlay = UIView(frame: view.frame)
        overlay!.backgroundColor = UIColor.black
        overlay!.alpha = 0.8
        
        //self.tabBarController?.tabBar.layer.zPosition = -1
        view.addSubview(overlay!)
    }
    
    func hideOverlay() {
        overlay?.removeFromSuperview()
        //self.tabBarController?.tabBar.layer.zPosition = 1
    }
    
    func showIndicator() {
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0);
        actInd.center = view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        view.addSubview(actInd)
        actInd.startAnimating()
    }
    
    func hideIndicator() {
        actInd.stopAnimating()
        actInd.removeFromSuperview()
    }
    
}
